package servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/administration")
public class AdministrationServlets extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {}
}
