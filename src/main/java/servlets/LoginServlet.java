package servlets;

import htmlGenerator.HtmlGenerator;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (
                request.getParameter("username") == null
                        || request.getParameter("username") == ""
                        || request.getParameter("password") == null
                        || (request.getParameter("username") != "guest" && request.getParameter("password") == "")
                ) {
            response.sendRedirect("registration.jsp");
        }
        HttpSession session = request.getSession();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        response.sendRedirect("index.jsp");
    }
}

