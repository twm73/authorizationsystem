package domain;

import java.util.ArrayList;
import java.util.List;

public class Role {
    String name;
    List<Permission> permissions = new ArrayList<Permission>();

    public Role(String name) {
        this.name = name;
    }

    public Role(String name, List<Permission> permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public Role setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
        return this;
    }

    public Role addPermissions (Permission permission){
        this.permissions.add(permission);
        return this;
    }
}
