package domain;

public interface DomainObject {
    long getId();
    void setId(long id);
}
