package db;

import java.sql.SQLException;

public interface Repository<TEntity> {
    TEntity withId(long id) throws SQLException;
    void add(TEntity entity) throws SQLException;
    void modify(TEntity entity) throws SQLException;
    void remove(TEntity entity) throws SQLException ;
}