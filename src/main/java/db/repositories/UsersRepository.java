package db.repositories;

import db.Repository;
import domain.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersRepository implements Repository<User> {

    private static List<User> db = new ArrayList<User>();

    @Override
    public User withId(long id) throws SQLException {
        return null;
    }

    @Override
    public void add(User user) throws SQLException {
        this.db.add(user);
    }

    @Override
    public void modify(User user) throws SQLException {

    }

    @Override
    public void remove(User user) throws SQLException {

    }

    public long count (){
        return db.size();
    }
}
