package db.repositories;

import db.Repository;
import domain.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RolesRepository implements Repository<Role> {

    private static List<Role> db = new ArrayList<Role>();

    @Override
    public Role withId(long id) throws SQLException {
        return null;
    }

    @Override
    public void add(Role role) throws SQLException {
        //db.add(new Role());
    }

    @Override
    public void modify(Role role) throws SQLException {

    }

    @Override
    public void remove(Role role) throws SQLException {

    }
    public Role getByName (String name){
        for(Role role: db){if(role.getName().equalsIgnoreCase(name)){return role;}}
        return null;
    }

    public long count (){
        return db.size();
    }
}
