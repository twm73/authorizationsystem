package db.repositories;
import db.Repository;
import domain.Permission;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PermissionRepository implements Repository<Permission> {

    private static List<Permission> db = new ArrayList<Permission>();

    @Override
    public Permission withId(long id) throws SQLException {
        return null;
    }

    @Override
    public void add(Permission permission) throws SQLException {
        db.add(permission);
    }

    @Override
    public void modify(Permission permission) throws SQLException {

    }

    @Override
    public void remove(Permission permission) throws SQLException {

    }

    public Permission getByName(String name){
        for (Permission permission: db){
            if (permission.getName().equalsIgnoreCase(name)){
                return permission;
            };
        }
        return null;
    }
    public long count (){
        return db.size();
    }
}
