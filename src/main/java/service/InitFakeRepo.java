package service;

import db.repositories.PermissionRepository;
import db.repositories.RolesRepository;
import domain.Permission;
import domain.Role;

import java.sql.SQLException;


public class InitFakeRepo {
    PermissionRepository permissionRepository;
    RolesRepository rolesRepository;

    public void init() throws SQLException {
        this.permissionRepository = new PermissionRepository();
        permissionRepository.add(new Permission("administrationTools"));
        permissionRepository.add(new Permission("premiumZone"));
        permissionRepository.add(new Permission("userProfile"));
        permissionRepository.add(new Permission("registration"));

        this.rolesRepository = new RolesRepository();
        rolesRepository.add(new Role("administrator").addPermissions(new Permission("administrationTools")));

    }
}
