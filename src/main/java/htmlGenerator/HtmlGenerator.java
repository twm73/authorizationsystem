package htmlGenerator;

public class HtmlGenerator {
    String headString;
    String cssString;
    String bodyString;
    String scriptString;

    public HtmlGenerator() {
        this.headString = "";
        this.bodyString = "";
        this.scriptString = "";
        this.cssString = "";
    }

    public void addCSS (String css){
        this.cssString += css;
    }

    public void addElement(String text, String tag){
        this.bodyString += "<" + tag + ">";
        this.bodyString += text;
        this.bodyString += "</" + tag + ">";
    }

    public String makePage (){
        String responseString ="";
        responseString += "<!DOCTYPE <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html lang=\"pl-PL\"><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>";
        responseString += this.cssString;
        responseString += "</style></head><body>";
        responseString += this.bodyString;
        responseString += "</body>";
        responseString += "<script>" + this.scriptString + "</script>";
        responseString += "</html>";
        return responseString;
    }
}
